#include "semaphore.h"
Semaphore::Semaphore(long count)
    : _count(count)
{
}

void
Semaphore::post()
{
    std::unique_lock<std::mutex> lock(_mutex);
    ++_count;
    _cv.notify_one();
}

int
Semaphore::wait(int msec)
{
    std::unique_lock<std::mutex> lock(_mutex);
    if (W_INFINITE == msec)
    {
        _cv.wait(lock);
    }
    else
    {
        if (!_cv.wait_for(lock, std::chrono::milliseconds(msec), [ = ] { return _count > 0; }))
        {
            return -1;
        }
    }

    --_count;
    return 0;
}
