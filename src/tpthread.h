/**
 * @file      thread.h
 * @copyright Copyright (c) 2019, CYG-ZCW Co., Ltd. All Rights Reserved.
 * @brief     brief
 * @author    caiwang213@qq.com
 * @date      2019-03-16 19:56:44
 *
 * @note
 *  thread.h defines
 */
#ifndef __TPTHREAD_H__
#define __TPTHREAD_H__
#include <mutex>
#include <condition_variable>

typedef void* (*THREADENTRY)(void *, int);
typedef std::condition_variable cv_t;

class PThread
{
public:
    PThread();
    virtual ~PThread();

    void               setEntry(THREADENTRY func, void * data = NULL, int id = 0);

    int                start();
    void               stop();

    bool               active();

protected:
    /* virtual void       run(); */
protected:
    static void      * entry(void *data, int id);

private:
    bool               _active;
    std::mutex         _mutex;
    cv_t               _cv;
    bool               _posted;

    THREADENTRY        _func;
    void             * _data;
    int                _id;

};

#endif
