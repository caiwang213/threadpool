/**
 * @file      xthread.h
 * @copyright Copyright (c) 2019, CYG-ZCW Co., Ltd. All Rights Reserved.
 * @brief     brief
 * @author    caiwang213@qq.com
 * @date      2019-03-16 00:07:19
 *
 * @note
 *  xthread.h defines
 */
#ifndef __XTHREAD_H__
#define __XTHREAD_H__
#include <thread>
typedef void* (*ROUTINE)(void *, int);

class xthread : public std::thread
{
public:
    xthread();

    template<typename T, typename...Args>
    xthread(uint32_t pid, T&&func, Args&&...args) : std::thread(std::forward<T>(func), std::forward<Args>(args)...)
    {
        _pid    = pid;
        _func   = NULL;
        _data   = NULL;
        _id     = 0;
        _join   = false;
        _detach = false;
    }

public:
    ROUTINE            func();
    void             * data();
    int                id();
    uint32_t           pid();
    uint64_t           pts();

    bool               is_join();
    bool               is_detach();

    void               set_entry(ROUTINE func, void *data, int id);
    void               set_join(bool join);
    void               set_detach(bool detach);
    void               set_pts(uint64_t pts);
private:
    uint32_t           _pid;
    bool               _join;
    bool               _detach;

    ROUTINE            _func;
    void             * _data;
    int                _id;
    uint64_t           _pts;
};
#endif
