/**
 * @file      threadpool.h
 * @copyright Copyright (c) 2019, CYG-ZCW Co., Ltd. All Rights Reserved.
 * @brief     brief
 * @author    caiwang213@qq.com
 * @date      2019-03-15 21:19:45
 *
 * @note
 *  threadpool.h defines
 */
#ifndef __THREADPOOL_H_
#define __THREADPOOL_H_
#include <queue>
#include <mutex>
#include <condition_variable>

#define MAX_INIT_SIZE     (100)
#define MAX_CAPA_SIZE     (300)

class xthread;

typedef std::queue<xthread *>   tqueue_t;
typedef std::condition_variable cv_t;

class ThreadPool
{
public:
    ThreadPool(int inisize = 10, int capacity = MAX_CAPA_SIZE);
    virtual ~ThreadPool();

    int                launch(void* (*routine)(void *, int), void *data = NULL, int id = 0);

protected:
    void               monitor();
    void               dispatch(uint32_t pid);

    static void      * entry(void *, int);
private:
    bool               _exit;
    uint32_t           _pids;

    int                _size;
    int                _capacity;

    tqueue_t           _idle;
    tqueue_t           _ready;

    std::mutex         _mutex;
    cv_t               _cv;
};
extern ThreadPool *tpool;
#endif
