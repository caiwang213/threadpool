#include "xthread.h"

xthread::xthread()
{
    _pid    = 0;
    _func   = NULL;
    _data   = NULL;
    _id     = 0;
    _join   = false;
    _detach = false;
    _pts    = 0;
}

void
xthread::set_entry(ROUTINE func, void *data, int id)
{
    _func = func;
    _data = data;
    _id   = id;
}

void
xthread::set_join(bool join)
{
    _join = join;
}

void
xthread::set_detach(bool detach)
{
    _detach = detach;
}

void
xthread::set_pts(uint64_t pts)
{
    _pts = pts;
}

bool
xthread::is_join()
{
    return _join;
}

bool
xthread::is_detach()
{
    return _detach;
}

ROUTINE
xthread::func()
{
    return _func;
}

void *
xthread::data()
{
    return _data;
}

int
xthread::id()
{
    return _id;
}

uint32_t
xthread::pid()
{
    return _pid;
}

uint64_t
xthread::pts()
{
    return _pts;
}
